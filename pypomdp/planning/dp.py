# -*- coding: utf-8 -*-
"""
    pypomdp.planning.dp
    ~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    :description: Dynamic programming POMDP planner

    Planner maximizes reward!
    
"""

DEBUG = True

import numpy

import pypomdp.planning
import pypomdp.model

class ValueIteration(pypomdp.planning.Planner):
    """
    Value Iteration POMDP Planner with finite horizon termination.

    :param model: POMDP model
    :type model: pypomdp.model.Model
    :param value_function: POMDP value function
    :type value_function: pypomdp.policy.ValueFunction
    """

    def __init__(self,model,value_function, discount = 1.0, **kwargs):
        self.model = model
        self.value_function = value_function
        self.discount = discount
        super(ValueIteration,self).__init__(**kwargs)
        self.__DEBUG = DEBUG



    def consider(self,node):
        """
        adds the given planning tree node to the search frontier
        :param node: planning tree node
        :type node: pypomdp.planning.PlanningTreeNode
        """
        raise NotImplementedError()


    def evaluate_belief(self,belief_state):
        """
        evaluate belief state and return value and policy as tuple

        :param belief_state: current belief state
        :type  belief_state: list of doubles

        :return: planning tree root node
        :rtype:  PlanningTreeNode
        """ 

        # initialize planning
        root = PlanningTreeNode(belief_state=belief_state,
                                 depth=0,
                                 parent_node = None,
                                 action_index = None,
                                 observation_index = None)
        root.parent_node = root
        nodes = [root]
        self.frontier = [root]
        depth_level = 0
        num_nodes = (self.model.num_actions * self.model.num_observs)**self.horizon

        #self.logger.debug("Rolling out %i nodes. up to horizon %i",num_nodes, self.horizon)

        while len(self.frontier):
            
            # logging
            if self.__DEBUG:
                logMsg = "epoch " + str(depth_level) + \
                    "; frontier size: " + str(len(self.frontier))
                print logMsg


            # get belief from frontier
            parent = self.frontier.pop()
            belief = parent.belief_state
            depth_level = parent.depth
            depth_level += 1
            
            # develop panning tree until horizon reached
            # then, evaluate leaf nodes and propagate value and policy through
            # planning tree up to rootNode
            
            for action_index in self.model.actions:
                intermediateBelief = self.model.update_belief_using_action(belief,action_index)
                for observation_index in self.model.observs:
                    next_belief = self.model.update_belief_using_action_observ(intermediateBelief, action_index,
                        observation_index)

                    # create node, append to planning tree and frontier
                    child = PlanningTreeNode(belief_state=next_belief,
                                                    depth=depth_level,
                                                    parent_node = parent,
                                                    action_index = action_index,
                                                    observation_index = observation_index)

                    nodes.append(child)
                    parent.addChild(child)
                    if self.consider(child):
                        self.frontier.append(child)

        # propagate value of leaf notes through tree to root node
        #self.logger.debug("Back propagation")
        self.evaluate(root)
        
        return root

    def evaluate(self,node):
        """
        set best action and value from children values
        propagated recursively
        """
        print "C:", node.children
        # leaf node evaluation by value function
        if not node.children:
            value = self.value_function.value(node.belief_state)
            return

        # find best child
        best_child_value = float("-inf")
        best_child = node.children[0]

        # select best child
        for child in node.children:
            self.evaluate(child)

            if (best_child_value < child.value):
                best_child_value = child.value
                best_child = child

        # reward:
        # immediate reward
        next_belief = self.model.update_belief_using_action_observ(node.belief_state,best_child.action_index,
            best_child.observation_index)
        imm_reward = 0 #self.model.belief_reward(belief, best_child.action_index, best_child.observation_index, next_belief)
        # accumulated reward
        node.value = imm_reward + self.discount #* best_child.value
        node.best_action_index = best_child.action_index


class FiniteHorizonValueIteration(ValueIteration):
    """
    Value Iteration POMDP Planner with finite horizon termination.

    :param model: POMDP model
    :type model: pypomdp.model.Model
    :param value_function: POMDP value function
    :type value_function: pypomdp.policy.ValueFunction
    :param horizon: planning horizon (default:2)
    :type horizon: int
    """
    def __init__(self,model,value_function, horizon=2):
        self.value_function = value_function
        self.horizon = horizon
        super(FiniteHorizonValueIteration,self).__init__(model,value_function)
        self.__DEBUG = DEBUG


    def consider(self,node):
        if (node.depth < self.horizon):
            return True

class InFiniteHorizonValueIteration(ValueIteration):
    """
    Value Iteration POMDP Planner with infinite horizon. Planning terminates if the
    influence of the leaf note value onto the root node value is less than epsilon

    :param model: POMDP model
    :type model: pypomdp.model.Model
    :param value_function: POMDP value function
    :type value_function: pypomdp.policy.ValueFunction
    :param horizon: planning horizon (default:2)
    :type horizon: int
    """
    def __init__(self,model,value_function, epsilon=0.1):
        self.value_function = value_function
        self.epsilon = epsilon
        super(InFiniteHorizonValueIteration,self).__init__(model,value_function)
        self.__DEBUG = DEBUG


    def consider(self,node):
        self.root.evaluate()
        if root.value / node.value > self.epsilon:
            self.frontier.append(node)



class PlanningTreeNode(object):

    """
    PlanningTreeNode

    attributes:
        beliefState - POMDP belief state
        value - value of belief state
        depth - planning depth level
        parent - parent PlanningTreeNode
        evaluationDepthLevel - depth of value evaluation
    """

    def __init__(self, belief_state, parent_node, depth, action_index, observation_index, value = None,
                 best_action_index = None, **kwargs):
        """
        initialize tree node

        arguments:
            beliefState
            parent - parent node
            depth - node depth in tree
        """
        self.belief_state           = belief_state
        self.parent_node            = parent_node
        self.depth                  = depth
        self.action_index           = action_index
        self.observation_index      = observation_index
        self.value                  = value
        self.best_action_index      = best_action_index
        self.evaluationDepthLevel   = None

        self._children = []

        self.__DEBUG = DEBUG

        super(PlanningTreeNode,self).__init__(**kwargs)

    def __repr__(self):
        msg = "bs:" + str(self.belief_state) +\
              " a:" + str(self.action_index) +\
              " o:" + str(self.observation_index) +\
              " v:" + str(self.value) +\
              " p:" + str(self.best_action_index) +\
              " d:" + str(self.depth) +\
              " c:" + str(len(self.children))

        return msg

    @property
    def children(self):
        return self._children

    def addChild(self,childNode):
        """
        add child node

        arguments:
            childNode - child planning tree node
            actionId - action index
            observationId - observation index
        """

        if self.__DEBUG:
            candidates = filter(lambda node: (node.action_index == childNode.action_index \
            and node.observation_index == childNode.observation_index),  self._children)
            if candidates:
                print candidates
                raise Exception("child for already exists for a: %i, o: %i: %s" % (childNode.action_index,
                    childNode.observation_index, candidates))

        self._children.append(childNode)

    def removeChild(self,actionIds=None, observationIds=None):
        """
        remove children by actionId

        arguments:
            actionIds - list of action indexes (default: range(actionCount))
            observationIds - list of observation indexes (default: range(observationCount))
        """

        if actionIds == None:
            actionIds = range(self.model.actionCount)
        if observationIds == None:
            observationIds = range(self.model.observationCount)

        candidates = filter(lambda node: node.actionId in actionIds\
        and node.observationId in observationIds, self._children)
        for candidate in candidates:
            self._children.remove(candidate)


