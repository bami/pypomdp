# -*- coding: utf-8 -*-
"""
    pypomdp.planning
    ~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

class Planner(object):
    def __init__(self,**kwargs):
        """
        initialize planner

        arguments:
            model - pomdp model
        """
        pass

    def evaluate_belief(self,beliefState):
        """
        evaluate belief state and return value and best action

        arguments:
            beliefState
        """
        pass

    def evaluateAction(self,actionId):
        """
        evaluate an action and return the corresponding alpha-vector
        """
        pass
