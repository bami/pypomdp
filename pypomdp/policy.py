# -*- coding: utf-8 -*-
"""
    pypomdp.policy
    ~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    :description: POMDP policy classes.
"""

import numpy
import math
from pypomdp.util.cassandra import *
from pypomdp.util.zmdp import *

EPSILON = 1e-6

class Policy(object):

    def __init__(self):
        super(Policy,self).__init__()


    def value(self, belief_state):
        """
        return the value for a belief state

        :param belief_state: belief state
        :return: value
        """
        raise NotImplemented()


    def action(self, belief_state):
        """
        returns the best action and it's value for the given belief

        :param belief_state: belief state
        :return:
        """
        raise NotImplemented()


class PolicyGraph(Policy):
    """
    Policy Graph Representation

    the state of a policy graph represent the best action and the transitions
    of the graph represent gathered. observations and next best action (sink).

    Hence, the best action is calculated from the last best action (source state)
    and the observation (transition index) as sink of the transition.

    self.transition[index][observation_index] = index
    self.actions[index] = action_index

    """

    @staticmethod
    def from_cassandra(policy_graph_string):
        """
        init from cassandra policy graph (pg) file

        :param policy_graph_string: policy graph string in Cassandra format
        :return: PolicyGraph
        """

        transitions, actions = read_cassandra_policy_graph(policy_graph_string)
        return PolicyGraph(transitions, actions)


    @staticmethod
    def read_cassandra(fileName):
        """
        initialize from cassandra policy graph file

        :param fileName: value function file name in Cassandra format
        :return: PolicyGraph
        """

        lines = None
        with open(fileName, 'r') as f:
            lines = f.readlines()

        return PolicyGraph.from_cassandra(lines)


    def __init__(self, transitions, actions, best_action = None):
        """
        initialize policy graph

        :param transitions: transition[index][observation_index] = index
        :param actions: actions[index] = action_index
        :param best_action: initial best action
        :return:
        """

        super(PolicyGraph,self).__init__()

        self.transition = transitions
        self.actions = actions
        self.last_action_index = best_action or self.actions[0]


    def action(self, belief_state, last_action_index,
                    observation_index):
        """
        returns the best action for the given belief

        :param belief_state: belief
        :param last_action_index: last applied action
        :param observation_index: obsservation
        :return: best action
        """

        if observation_index == -1:
            return self.last_action_index, None

        state = self.actions.index(last_action_index)
        next_state = self.transition[state][observation_index]
        next_action = self.actions[next_state]
        self.last_action_index = next_action

        return next_action, None


class AlphaVectorValueFunction(Policy):
    """
    maximum plane (lower bound) value function

    can only deal with rewards!!!

    self._alphaVectors
    self._alphaVectorActions

    alpha vector - represents the value of the corresponding action at the MDP states

    Evalutation with one-step-backup if belief and actions is given
    """

    """ value type """
    type = numpy.double

    @staticmethod
    def initFromCassandraFile(fileName, POMDPModel):
        """
        initialize from cassandra alpha file

        :param fileName: value function file name (Cassandra format)
        :param POMDPModel: POMDP model
        :type POMDPModel: pypomdp.model.POMDP
        :return: value function
        """

        # read file
        lines = None
        with open(fileName, 'r') as f:
            lines = f.readlines()
        alpha_vector_actions, alpha_vectors = read_cassandra_alpha_policy(lines)
        vf = AlphaVectorValueFunction(POMDPModel, alpha_vectors, alpha_vector_actions)
        return vf


    @staticmethod
    def init_from_zmdp_file(filename,POMDPModel):
        """
        initialize from ZMDP policy file

        :param filename: file name of zmdp poilicy file
        :param POMDPModel: pomdp model
        :return: value function
        """

        alpha_vector_actions, alpha_vectors = read_zmdp_policy(filename, POMDPModel.num_states)
        vf = AlphaVectorValueFunction(POMDPModel, alpha_vectors, alpha_vector_actions)
        return vf


    def __init__(self, model, alphaVectors, alphaVectorActions):
        """
        initialize value function

        :param model: pomdp model
        :type model: pypomdp.model.POMDP
        :param alphaVectors: list of alpha vectors (MDP values)
        :param alphaVectorActions: list of corresponding actions
        :return:
        """
        super(AlphaVectorValueFunction,self).__init__()

        self.model = model

        # type filtering
        self._alphaVectors = [numpy.array(v, dtype = AlphaVectorValueFunction.type)
                              for v in alphaVectors]
        self._alphaVectorActions = [int(actionId)
                                    for actionId in alphaVectorActions]
        self.validate()


    def evaluate(self, belief_state):
        """
        returns the best action and it's value for the given belief
        from alpha vectors

        :param belief_state: belief
        :return: (action, value)
        """

        # filter applicable alpha-vector planes
        # a plane is applicable to belief_state, if it is defines in any dimension
        # for which the belief state is non zero

        assert len(belief_state) == self.model.num_states, "invalid belief dimensions"
        applicable_alphas = [] # list of (alpha,actionid)

        # filter applicable planes
        for plane, action in zip(self._alphaVectors, self._alphaVectorActions):
            for a,b in zip(plane, belief_state):
                if numpy.isnan(a) and b > 0.0:
                    break
            else:
                applicable_alphas.append((action,plane))

        if not applicable_alphas:
            raise Exception("no plane applicable to belief %s" % belief_state)

        best_action = applicable_alphas[0][0]
        best_value = sum([p*b for p,b in zip(applicable_alphas[0][1], belief_state)
                          if b > 0.0])
        for action, plane in applicable_alphas:
            # value as cross product based on relevant dimensions
            value = sum([p*b for p,b in zip(plane, belief_state) if b > 0.0])
            if value > best_value:
                best_action = action
                best_value = value

        return best_action, best_value


    def action(self, belief_state):
        """
        return the policy for a belief state

        :param belief_state: belief state
        :return: action
        """

        return self.evaluate(belief_state)[0]


    def value(self, belief_state):
        """
        return the value for a belief state

        :param belief_state: belief state
        :return: value
        """

        return self.evaluate(belief_state)[1]


    def validate(self):
        """
        check consistency i.t.o.
        - check if length of alpha vectos is consistent
        - check if alpha vector's actions ids correspont to model
        - check if alpha vector's actions count correspont to alpha vectors

        """

        assert len(self._alphaVectorActions) == len(self._alphaVectors)

        stateCount = self.model.num_states
        actionCount = self.model.num_actions

        for vector in self._alphaVectors:
            assert vector.size == stateCount, "invalid alpha vector. Size mismatch %i %i" % (vector.size, stateCount)

        for actionNo in self._alphaVectorActions:
            if not actionNo in range(actionCount):
                raise Exception("invalid action found")


    def add(self,vector,actionId):
        """
        add alpha vector

        :param vector: mdp state values
        :param actionId: action
        :return:
        """

        assert len(vector) == self.model.num_states, "wrong vector size"
        assert actionId in range(self.model.num_actions), "invalid action id"

        self._alphaVectors.add(numpy.array(vector,AlphaVectorValueFunction.type))
        self._alphaVectorActions(actionId)

    def prune(self):
        """
        remove unused alpha vectors
        """

        raise NotImplemented()

    def is_applicable(self,beliefState,alpha_vector):
        """
        return if the alpha vector is applicable to belief state.

        it is defined for those beliefs b such that the non-zero
        entries of b are a subset of the entries present in the plane's alpha
        vector.  If this condition holds we say the plane is 'applicable' to b.

        :param beliefState: belief
        :param alpha_vector: alpha vector
        :return: Boolean
        """

        # l contains Booleans, that indicate the belief state dimension
        # that matters and a non valid alpha value in that dimension
        l = [bs > EPSILON and math.isnan(a) for bs, a in zip(beliefState,alpha_vector)]

        if not True in l:
            return True

        return False


    def applicable_alphas(self,beliefState):
        """
        filter applicable alpha planes for belief state b

        it is defined for those beliefs b such that the non-zero
        entries of b are a subset of the entries present in the plane's alpha
        vector.  If this condition holds we say the plane is 'applicable' to b.

        :param beliefState: belief
        :return: ([applicable alphas],[actions])
        """

        applicable_alphas = []
        applicable_alpha_actions = []

        for alpha_vector_index in range(len(self._alphaVectors)):
            alpha = self._alphaVectors[alpha_vector_index]
            if self.is_applicable(beliefState,alpha):
                action_id = self._alphaVectorActions[alpha_vector_index]
                applicable_alphas.append(alpha)
                applicable_alpha_actions.append(action_id)

        return applicable_alphas, applicable_alpha_actions