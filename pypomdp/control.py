# -*- coding: utf-8 -*-
"""
    pypomdp.pierre
    ~~~~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import logging
import pypomdp.model
import pypomdp.policy

EPSILON = 1e-6
DEBUG = True

class Controller(object):

    @staticmethod
    def null_observation(belief_state, oblivion_level):
        """
        apply null observation to belief, return new belief

        :param belief_state:
        :param oblivion_level:
        :return:
        """

        import numpy

        belief = numpy.array(belief_state)

        # generate uniform belief
        p = 1. / len(belief_state)
        uniform_belief = [p] * len(belief_state)

        offset = uniform_belief - belief
        oblivion_step = oblivion_level * offset

        # move belief towards uniform belief and normalize
        new_belief = belief + oblivion_step
        new_belief /= new_belief.sum()
        next_belief = new_belief.tolist()
        return next_belief


    def __init__(self):
        super(Controller,self).__init__()


    def senseact(self,observ):
        """
        sense observation and returns best action according to policy

        :param observ: observation
        :return: action index
        """
        raise NotImplementedError()


class ProdController(Controller):

    def __init__(self, model, policy, logger = None, initial_belief = None,
                 initial_best_action = None, epoch_length = None,
                 oblivion_level = None):
        """
        Controller you need to trigger externally via senseact()

        :param model: model
        :type model: pypomdp.model.POMDP
        :param policy: policy
        :type policy: pypomdp.policy.Policy
        :param initial_belief: initial belief (default: uniform belief)
        :type initial_belief: [float,..] according to #states
        :param initial_best_action: best action index
        :type initial_best_action: int
        :param epoch_length: epoch length [s] (default:inf)
        :param logger: logger interface
        :type logger: logging.Logger
        :param oblivion_level: level of oblivion of NULL observations (default: 0.)
        :type oblivion_level: float (range: 0-1)
        :return: controller object
        """

        # sanity checks
        assert isinstance(model, pypomdp.model.POMDP), "Invalid model type"
        assert isinstance(policy, pypomdp.policy.Policy), "Invalid policy type"

        Controller.__init__(self)

        self.logger = logger or logging
        self.model = model
        self.policy = policy
        self.oblivion_level = oblivion_level or 0
        self.epoch_length = epoch_length or float('inf')

        # initialize controller state
        self.accumulated_reward = 0.
        self.last_observ = None
        self.belief = initial_belief or [1./model.num_states] * model.num_states
        self.last_action = initial_best_action or self.policy.action(self.belief)

        self.logger.debug("POMDP Controller initialized: %s" % self.status())

    def senseact(self, observ):
        """
        sense observation and returns best action according to policy

        :param observ: observation
        :return: action index
        """

        last_belief = self.belief

        self.logger.debug("o: %i" % observ)

        self.last_observ = observ
        # update belief from observation
        self.belief = self.model.update(self.belief,
            self.last_action, observ)
        # ask policy for action
        action = self.policy.action(self.belief)
        self.last_action = action
        # apply policy to belief
        self.belief = self.model.process_update(self.belief, action)

        self.logger.debug("%s + o:%i,a:%i -> %s" %
                          (last_belief, observ, action, self.belief))
        return action

    def status(self):
        """
        returns status string
        :return:
        """

        return ("""
        belief :%s,
        last action: %i,
        last observ: %s"
        """ % (self.belief,self.last_action,str(self.last_observ)))
