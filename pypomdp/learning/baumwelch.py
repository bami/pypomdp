# -*- coding: utf-8 -*-
"""
    pypomdp.learning
    ~~~~~~~~~~~~~~~~

    :copyright: (c) 2011, 2013 by Daniel Mescheder, Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: implementation based on
    http://danielmescheder.wordpress.com/2011/12/05/training-a-pomdp-with-python/
"""

import numpy as np

class MarkovModel(object):
    def __init__(self,alist,c,init):
        """
        :param alist: transition model indexed by action
        :param c: observation model c[o][s] = P(s|o)
        :param init: initial belief
        """

        self.alist = alist # transition matrix
        self.c = c # observation matrix
        self.ns = c.shape[1] # number of states
        self.os = c.shape[0] # number of observations
        self.inps = len(alist) # number of actions
        self.init = init # initial state

    @staticmethod
    def random_model(self, inps, ns, os):
        """
        generate a random model according to a given number of actions,
        states and observation

        :param inps: number of action
        :param ns: number of states
        :param os: number of observations
        """


class BaumWelchLearner(object):

    """
    POMDP learner

    tableau[timestep][state] - alpha, beta, N

    """

    def __init__(self, m):
        """

        :param m: model
        :type m: MarkovModel
        :return:
        """
        self.m = m


    def make_tableaus(self,xs,ys):
        """
        Generate tableaus for a given sequence of inputs xs, observations ys and
        a model assumption m.

        :param xs: sequence of actions
        :param ys: sequence of observations
        :param m: model
        :type m: MarkovModel
        :return: alpha, beta, N
        """

        alpha = np.zeros((len(ys),self.m.ns))
        beta  = np.zeros((len(ys),self.m.ns))
        gamma = np.zeros((len(ys),self.m.ns))
        N     = np.zeros((len(ys),1))

        # Initialize:
        gamma[0:1,:] = self.m.init.T * self.m.c[ys[0]:ys[0]+1,:]
        N[0,0] = 1 / np.sum(gamma[0:1,:])
        alpha[0:1,:] = N[0,0]*gamma[0:1,:]
        beta[len(ys)-1:len(ys),:] = np.ones((1,self.m.ns))

        for i in range(1,len(ys)):
            gamma[i:i+1,:] = self.m.c[ys[i]:ys[i]+1,:] * \
                             np.sum((self.m.alist[xs[i-1]].T*alpha[i-1:i,:].T),axis=0)
            N[i,0] = 1 / np.sum(gamma[i:i+1,:])
            alpha[i:i+1,:] = N[i,0]*gamma[i:i+1,:]
        for i in range(len(ys)-1,0,-1):
            beta[i-1:i] = N[i] * np.sum(self.m.alist[xs[i-1]]
                               * (self.m.c[ys[i]:ys[i]+1,:] * beta[i:i+1,:]).T,axis=0)

        return alpha,beta,N


    @staticmethod
    def likelihood(tableaus):
        alpha,beta,N = tableaus
        return np.product(1/N)


    @staticmethod
    def log_likelihood(tableaus):
        alpha,beta,N = tableaus
        return -np.sum(np.log(N))


    def state_estimates(self, xs, ys, tableaus=None):
        """
        Calculate the posterior distribution over the given sequence of inputs
        xs and a sequence of observations ys that correspond to xs.

        :param xs: sequence of actions
        :param ys: sequence of observations
        :param m: model
        :type m: MarkovModel
        :param tableaus: (alpha,beta,N)
        :return: belief
        """

        alpha,beta,N = tableaus or self.make_tableaus(xs,ys,self.m)
        return alpha*beta


    def transition_estimates(self, xs, ys, tableaus=None):
        """
        Given a sequence of inputs xs and a sequence of observations ys that
        correspond to xs, estimate the probability of transferring between
        two states at each time step.

        :param xs: sequence of actions
        :param ys: sequence of observations
        :param m: model
        :type m: MarkovModel
        :param tableaus: (alpha,beta,N)
        :return: state transitions estimate
        """

        alpha,beta,N = tableaus or self.make_tableaus(xs,ys,self.m)

        result = np.zeros((self.m.ns,self.m.ns,len(ys)))
        for t in range(len(ys)-1):
            a = self.m.alist[xs[t]]
            result[:,:,t] = a*alpha[t:t+1,:]*self.m.c[ys[t+1]:ys[t+1]+1,:].T*beta[t+1:t+2,:].T*N[t+1,0]
        a=self.m.alist[xs[len(ys)-1]]
        result[:,:,len(ys)-1] = a*alpha[-1:,:]
        return result


    def stateoutput_estimates(self, xs, ys, m, sestimate=None):
        """
        Takes an input sequence and an output sequence and for each time step
        computes the posterior probability of being in a state and observing a
        certain output.

        :param xs: sequence of actions
        :param ys: sequence of observations
        :param m: model
        :type m: MarkovModel
        :param sestimate: belief
        :return:
        """

        if sestimate is None: sestimate = self.state_estimates(xs,ys,m)
        result = np.zeros((m.os,m.ns,len(ys)))
        for t in range(len(ys)):
            result[ys[t]:ys[t]+1,:,t] = sestimate[t:t+1,:]
        return result


    def improve_params(self, xs, ys, m, tableaus=None):
        """
        Baum-Welch model estimation update

        Calculate:
        * The posterior state estimates for each
        * The posterior transition estimates for each
        * The posterior joint state/output estimates for each

        :param xs: sequence of actions
        :param ys: sequence of observations
        :param m: model
        :type m: MarkovModel
        :param tableaus:
        :return: transition_model[action], observation_model[o][s]
        """

        if tableaus is None: tableaus = self.make_tableaus(xs,ys,m)
        estimates = self.state_estimates(xs,ys,m,tableaus=tableaus)
        trans_estimates = self.transition_estimates(xs,ys,m,tableaus=tableaus)
        sout_estimates = self.stateoutput_estimates(xs,ys,m,sestimate=estimates)

        # Calculate the numbers of each input in the input sequence.
        nlist = [0]*m.inps
        for x in xs: nlist[x] += 1

        sstates = [np.zeros((m.ns,1)) for i in range(m.inps)]
        for t in xrange(len(ys)): sstates[xs[t]] += estimates[t:t+1,:].T/nlist[xs[t]]

        # Estimator for transition probabilities
        alist = [np.zeros_like(a) for a in m.alist]
        for t in xrange(len(ys)):
            alist[xs[t]] += trans_estimates[:,:,t]/nlist[xs[t]]
        for i in xrange(m.inps):
            alist[i] = alist[i]/(sstates[i].T)
            np.putmask(alist[i],(np.tile(sstates[i].T==0,(m.ns,1))),m.alist[i])

        c = np.zeros_like(m.c)
        for t in xrange(len(ys)):
            x = xs[t]
            c += sout_estimates[:,:,t]/(nlist[x]*m.inps*sstates[x].T)
        # Set the output probabilities to the original model if
        # we have no state observation at all.
        sstatem = np.hstack(sstates).T
        mask = np.any(sstatem == 0,axis=0)
        np.putmask(c,(np.tile(mask,(m.os,1))),m.c)

        return alist,c
