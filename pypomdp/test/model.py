# -*- coding: utf-8 -*-
"""
    pypomdp.test.model
    ~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import unittest
import tempfile

import pypomdp.model

class TestModel(unittest.TestCase):

    test_pomdp_num_states        = 3
    test_pomdp_states            = list(xrange(test_pomdp_num_states))
    test_pomdp_state_names       = ['s_left','s_center','s_right']

    test_pomdp_num_actions       = 3
    test_pomdp_actions           = list(xrange(test_pomdp_num_actions))
    test_pomdp_action_names      = ['a_left','a_no','a_right']

    test_pomdp_num_observations  = 3
    test_pomdp_observations      = list(xrange(test_pomdp_num_observations))
    test_pomdp_observation_names = ['o_left','o_center','o_right']

    test_pomdp = """
    discount: 0.950000
    values: reward

    # actions
    actions: a_left a_no a_right

    # states
    states: s_left s_center s_right

    # observations
    observations: o_left o_center o_right

    #transition model P(S'|S,A)
    T:0:0:0 0.516658 
    T:0:0:1 0.039855 
    T:0:0:2 0.443487 
    T:0:1:0 0.016952 
    T:0:1:1 0.006461 
    T:0:1:2 0.976587 
    T:0:2:0 0.037205 
    T:0:2:1 0.034600 
    T:0:2:2 0.928195 

    T:1:0:0 0.556127
    T:1:0:1 0.041269
    T:1:0:2 0.402604 
    T:1:1:0 0.023261
    T:1:1:1 0.003421
    T:1:1:2 0.973318 
    T:1:2:0 0.032625 
    T:1:2:1 0.029523 
    T:1:2:2 0.937852 

    T:2:0:0 0.596931 
    T:2:0:1 0.042505 
    T:2:0:2 0.360564 
    T:2:1:0 0.026530
    T:2:1:1 0.006689
    T:2:1:2 0.966781 
    T:2:2:0 0.028885
    T:2:2:1 0.024784
    T:2:2:2 0.946331 

    # observation model S x O = P(O|S)
    O:*:0:0 0.440106 
    O:*:0:1 0.481309
    O:*:0:2 0.078585 
    O:*:1:0 0.157552 
    O:*:1:1 0.817827 
    O:*:1:2 0.024622 
    O:*:2:0 0.051093
    O:*:2:1 0.872600
    O:*:2:2 0.076307 


    #reward model R: <action> : <start-state> : <end-state> : <observation>
    R:0:*:0:* -3.0
    R:0:*:1:* -1.0
    R:0:*:2:* -3.0
    R:1:*:0:* -2.0
    R:1:*:1:* 0.0
    R:1:*:2:* -2.0
    R:2:*:0:* -3.0
    R:2:*:1:* -1.0
    R:2:*:2:* -3.0
    """

    def setUp(self):
        # Create temp file for read_cassandra test
        self.tmp_file = tempfile.NamedTemporaryFile()
        # Write test POMDP
        self.tmp_file.write(TestModel.test_pomdp)
        # Flush I/O buffer
        self.tmp_file.file.flush()
        # Reset seek point to start
        self.tmp_file.seek(0)

    def test_read_from_cassandra(self):
        pomdp = pypomdp.model.POMDP.read_cassandra(self.tmp_file.name)

        # Test states
        self.assertEqual(pomdp.num_states,
                self.test_pomdp_num_states,
                "expected number of states does not match actual number")
        self.assertEqual(pomdp.states,
                self.test_pomdp_states,
                "expected states do not match actual states")
        self.assertEqual(pomdp.state_names,
                self.test_pomdp_state_names,
                "expected states names do not match actual names")
        # Test actions
        self.assertEqual(pomdp.num_actions,
                self.test_pomdp_num_actions
                ,"expected number of actions does not match actual number")
        self.assertEqual(pomdp.actions,
                self.test_pomdp_actions,
                "expected states do not match actual actions")
        self.assertEqual(pomdp.action_names,
                self.test_pomdp_action_names,
                "expected action names do not match actual names")
        # Test observations
        self.assertEqual(pomdp.num_observs,
                self.test_pomdp_num_observations,
                "expected number of observation dos not match actual number")
        self.assertEqual(pomdp.observs,
                self.test_pomdp_observations,
                "expected states do not match actual observations")
        self.assertEqual(pomdp.observ_names,
                self.test_pomdp_observation_names,
                "expected observation names do not match actual names")



    def tearDown(self):
        self.tmp_file.close()

