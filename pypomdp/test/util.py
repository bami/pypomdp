# -*- coding: utf-8 -*-
"""
    pypomdp.test.util
    ~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import unittest

import pypomdp.util

class TestUtil(unittest.TestCase):

    def test_check_matrix_dimensions(self):

        mx = [[1,2,3],[4,4,5]]
        pypomdp.util.check_matrix_dimensions(mx,2,3)
        with self.assertRaises(ValueError):
            pypomdp.util.check_matrix_dimensions(mx,2,4)

        mx = [[1,2,3],[4,5,6,6]]
        with self.assertRaises(ValueError):
            pypomdp.util.check_matrix_dimensions(mx,2,3)

        mx = [[[1,2],[3,4],[4,4]],[[0,4],[1,3],[3,2]]]
        pypomdp.util.check_matrix_dimensions(mx,2,3,2)

        mx = [[[1,2],[3,4],[4,4,8]],[[0,4],[1,3],[3,2]]]
        with self.assertRaises(ValueError):
            pypomdp.util.check_matrix_dimensions(mx,2,3,2)

        dims = (8,3,1,2,3,4)
        mx = pypomdp.util.create_null_array(None,*dims)
        pypomdp.util.check_matrix_dimensions(mx,*dims)
        with self.assertRaises(ValueError):
            pypomdp.util.check_matrix_dimensions(mx,10)

    def test_create_null_array(self):
        mx = pypomdp.util.create_null_array(None,2,3)
        self.assertEqual(mx,[[None,None,None],[None,None,None]],
                'created array does not match expected array')

    def test_alist_to_dict(self):
        alist1 = [('a',[1,2,3])
                 ,('b',None)
                 ,('c',('f',))
                 ,('b',[1,2,3])
                 ,('a',(1,))]
        dict1 = pypomdp.util.alist_to_dict(alist1)

        xdict1 = {'a': [[1,2,3],(1,)]
                 ,'b': [None,[1,2,3]]
                 ,'c': [('f',)]}

        self.assertEqual(xdict1,dict1,'created dict does not match expected')




