# -*- coding: utf-8 -*-
"""
    pypomdp.test.paser
    ~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import unittest

import pypomdp.parser as parser

class TestParser(unittest.TestCase):
    test_pomdp = """
    # comment 1
    # comment 2
    discount:    0.99
    values: reward  
    states: 2
    actions: go_left go_right    
    observations: obs0   obs1

    T : 0 : 0 :0 0.65
    T :0 : 0 :1   0.35
    T : 0           : 1 : 0  0.45
    T: 0 : 1:   1   0.55
    T    : 1 : 0: * 0.50
    T : 1 : 1: * 0.50
    O: 0 :     * : 0 0.50
    O :    0 :  0 : 1 0.78
    O: 0 :1 :     1 0.12
    R   : * : * :    * : * 1000
    R: 0 : 0:1:1 -77.33
    R:1:1:1:* 13
    """
    test_pomdp_2 = """
    # comment 1
    # comment 2
    discount:    0.99
    values: reward  
    states: 2
    actions: 2
    observations: 2

    T:*:*:* 0.1
    O:*:*:* 0.1

    R:1:1:1:* 13
    """

    def setUp(self):
        self.test_pomdp = TestParser.test_pomdp

    def test_parser(self):
        m = parser.model_from_string(self.test_pomdp)

        self.assertEqual(2,m.num_states)
        self.assertEqual(2,m.num_actions)
        self.assertEqual(2,m.num_observations)

        self.assertEqual(["state0","state1"],m.state_names)
        self.assertEqual(["go_left","go_right"],m.action_names)
        self.assertEqual(["obs0","obs1"],m.observation_names)

        self.assertEqual(0.99,m.discount)
        self.assertEqual("reward",m.values)

        self.assertEqual(0.65,
                m.transition_probabilities[0][0][0])
        self.assertEqual(0.35,
                m.transition_probabilities[0][0][1])
        self.assertEqual(0.45,
                m.transition_probabilities[0][1][0])
        self.assertEqual(0.55,
                m.transition_probabilities[0][1][1])
        self.assertEqual(0.50,
                m.transition_probabilities[1][0][0])
        self.assertEqual(0.50,
                m.transition_probabilities[1][0][1])
        self.assertEqual(0.50,
                m.transition_probabilities[1][1][0])
        self.assertEqual(0.50,
                m.transition_probabilities[1][1][1])
        self.assertEqual(0.50,
                m.observation_probabilities[0][0][0])
        self.assertEqual(0.50,
                m.observation_probabilities[0][1][0])
        self.assertEqual(0.78,
                m.observation_probabilities[0][0][1])
        self.assertEqual(1000,
                m.rewards[0][0][0][0])
        self.assertEqual(1000,
                m.rewards[0][1][1][1])
        self.assertEqual(-77.33,
                m.rewards[0][0][1][1])
        self.assertEqual(13,
                m.rewards[1][1][1][0])
        self.assertEqual(13,
                m.rewards[1][1][1][1])

    def test_parser_2(self):
        m = parser.model_from_string(self.test_pomdp_2)

        self.assertEqual(13,
                m.rewards[1][1][1][0])
        self.assertEqual(13,
                m.rewards[1][1][1][1])



    def test_is_whitespace(self):
        t1 = "    "
        self.assertTrue(parser.is_whitespace(t1))
        t2 = "\t"
        self.assertTrue(parser.is_whitespace(t2))
        t3 = "\t\r"
        self.assertTrue(parser.is_whitespace(t3))

        f1 = "   x "
        self.assertFalse(parser.is_whitespace(f1))
        f2 = "abc"
        self.assertFalse(parser.is_whitespace(f2))
        f3 = "\n\n  t\n"
        self.assertFalse(parser.is_whitespace(f3))
    
    def test_is_comment(self):
        t1 = "#"
        self.assertTrue(parser.is_comment(t1))
        t2 = " # this is a comment!!"
        self.assertTrue(parser.is_comment(t2))
        t3 = "# @  @ #@@ @###"
        self.assertTrue(parser.is_comment(t3))
        
        f1 = "sdas"
        self.assertFalse(parser.is_comment(f1))
        f2 = ""
        self.assertFalse(parser.is_comment(f2))
        f3 = "T: 10 : 10 : 10 0.33"
        self.assertFalse(parser.is_comment(f3))





