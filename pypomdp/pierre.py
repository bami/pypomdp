# -*- coding: utf-8 -*-
"""
    pypomdp.pierre
    ~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import numpy
import pypomdp.model
import pypomdp.policy

from pierre.control import Controller
from pierre.control import schedule
from pierre.io import IOInterface
from pierre.scheduling import EveryNSeconds, EveryNMilliSeconds


EPSILON = 1e-6
DEBUG = True

class POMDPController(Controller):
    """
    POMDP Controller

    The POMDP controller runs a control loop, that updates the belief state
    according to a sensed observation, and applies a control action which
    is queried from a pypodmp.policy.Policy

    The implementation is based on the pierre framework.

    :param io_interface: IO-interface responsible for mapping (real space
                         to/from controller space) and signaling.
    :type io_interface: :py:class:`pierre.io.IOInterface`

    :param model:       POMDP model definition
    :type model:        :py:class: `pypomdp.Model`

    :param policy:      Control policy
    :type policy:       :py:class: `pypomdp.Policy`

    :param initial_belief: initial belief state
    :type initial_belief:       :py:class: `list` (if none is given, a uniformed belief
                        state is created)

    :param  best_action: initial best action (default: from policy)
    :type best_action:          :py:Integer:
    :param scheduler: optional scheduler (if none is given, a new
                      :py:class:`pierre.scheduling.Scheduler` is created).
    :type scheduler: :py:class:`pierre.scheduling.Scheduler`

    :param oblivion_level: level of oblivion [0,1] (default 0.5)
    :type oblivion_level: :py:Double:

    """

    def __init__(self,
                 io_interface,
                 model,
                 policy,
                 initial_belief=None,
                 best_action = None,
                 scheduler=None,
                 oblivion_level = 0.5):
        
        # sanity checks
        assert isinstance(model, pypomdp.model.POMDP), "Invalid type"
        assert isinstance(policy, pypomdp.policy.Policy), "Invalid type"

        super(POMDPController,self).__init__(io_interface,scheduler)
        
        self.model = model
        self.policy = policy
        self.oblivion_level = oblivion_level

        # initialize controller state
        self.accumulated_reward = 0.
        self.last_observation_index = None
        self.current_belief = initial_belief or [1./model.num_states] * model.num_states
        self.best_action_index = best_action or self.policy.best_action(self.current_belief,0,0)[0]

        self.logger.debug("POMDP Controller initialized: %s" % self.status())

    @schedule(EveryNMilliSeconds(600))
    def sense_act(self):
        """
        The controller's sense and act method
        """
        self.sense()
        self.act()


    def sense(self):
        """
        The controller's sense method.

        gather observation_index index from io-interface
        -- ignore NONEs
        -- else apply control loop: observation_index model, best action, sys dynamics
        """
        observation_index = self.io_interface.sense()
        self.logger.debug('Controller sensed: %s' % str(observation_index))

        if observation_index is None: # ignore invalid observations
            return

        self.last_observation_index = observation_index
        last_belief = self.current_belief

        # apply observation
        if observation_index == -1: # null observ
            intermediate_belief = self.update_belief_using_null_observation(last_belief)
        else: # observ from model
            intermediate_belief = self.model.update_belief_using_action_observ(
                self.current_belief, self.best_action_index,self.last_observation_index)

        # get best action for next belief
        self.best_action_index, value = self.policy.best_action(intermediate_belief,
            self.best_action_index,
            observation_index)

        # apply process model
        self.next_belief = self.model.update_belief_using_action(intermediate_belief,
            self.best_action_index)

        # update reward
        immediate_reward = 0.0
        r = self.model.rws[self.best_action_index]
        for source_index in self.model.num_states:
            for sink_index in self.model.num_states:
                source_reward = self.last_belief[source_index] * \
                                r[source_index][sink_index][observation_index]
                sink_reward = self.next_belief[sink_index] * \
                              r[source_index][sink_index][observation_index]
                immediate_reward += source_reward + sink_reward

        self.accumulated_reward += immediate_reward


    def act(self):
        """
        The controller's act method.
        """
        self.logger.debug('Controller acted: %i' % self.best_action_index)
        self.io_interface.act(self.best_action_index)


    @schedule(EveryNSeconds(1))
    def statusInfo(self):
        self.logger.debug(self.status())

    def status(self):
        """
        returns status string
        """
        return_string = ""
        return_string += "\nBelief: "
        return_string += str(self.current_belief)
        return_string += "\nbest action: '%s' (%i)" % (self.bestAction(),self.best_action_index)
        return_string += "\nlast observation: (" + str(self.last_observation_index) + ")"
        return_string += "\naccumulated reward: %f" % self.accumulated_reward
        return return_string

    def bestAction(self):
        """
        return name of best action
        """
        return self.model.actionNames[self.best_action_index]

    def update_belief_using_null_observation(self, belief_state):
        """
        apply null observation to belief, return new belief
        """
        belief = numpy.array(belief_state)

        # generate uniform belief
        p = 1. / self.model.num_states
        uniform_belief = [p] * self.model.num_states

        offset = uniform_belief - belief
        oblivion_step = self.oblivion_level * offset

        # move belief towards uniform belief and normalize
        new_belief = belief + oblivion_step
        new_belief /= new_belief.sum()
        next_belief = new_belief.tolist()
        return next_belief

