# -*- coding: utf-8 -*-
"""
    pypomdp.simulator
    ~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    :description: POMDP simulation module.

    The POMDP simulator probabilistically simulates the system behavior
    given as pypomdp.model.POMDP
"""
import random


class Simulator(object):

    def __init__(self, pomdp, init_state = None):
        """
        POMDP Simulator

        :param pomdp: pomdp model
        :type pomdp: pypomdp.model.POMDP
        :param init_state: initial system state (default: random)
        :return:
        """

        self.pomdp = pomdp
        self.current_state =  init_state or int(random.uniform(0,pomdp.num_states))


    def simulate(self,action):
        """
        simulate system: apply action

        :param action: action
        :type action: int
        :return: (reward, observ)
        """

        current_state = self.current_state
        next_state    = -1

        print('Currently in state: %s [%u]' %
                (self.pomdp.state_names[current_state]
                ,current_state))

        print('Taking action: %s [%u]' %
                (self.pomdp.action_names[action]
                ,action))


        # Probabilities of reach sink states
        sink_probs = self.pomdp.tps[action][current_state]

        # sample sink state
        r = random.random()
        a = 0.
        for sink in self.pomdp.states:
            a += sink_probs[sink]
            if a >= r:
                next_state = sink
                break
        print('Transitioned to state: %s [%u]' %
            (self.pomdp.state_names[next_state],next_state))


        # Observation probabilities
        obs_probs = self.pomdp.ops[action][next_state]
        # sample observation
        r = random.random()
        a = 0.0
        for obs in self.pomdp.observs:
            a += obs_probs[obs]
            if a >= r:
                observation = obs
                break
        print('Perceived observation: %s [%u]' %
                (self.pomdp.observ_names[observation]
                ,observation))

        # Reward
        reward = self.pomdp.rws[action][current_state][next_state][observation]
        print('Received reward: %f\n' % reward)

        # update state
        self.current_state = next_state

        return reward, observation
