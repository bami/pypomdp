# -*- coding: utf-8 -*-
"""
    pypomdp.visualizition
    ~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
"""
import numpy
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d
from matplotlib.lines import Line2D
from matplotlib.axes import Axes

_Action_color = ['b','g','r','c','y','m','k']

def plot_ValueFunction_2D(vf,state_indexes=[0,1]):
    """
    plot alpha vectors using matplotlib
    arguments:
        value_function [pypomdp.ValueFunction]
        state_indexes - 2 state indexes (default: [0,1])
    """
    fig = plt.figure()
    ax = Axes(fig, [.1,.1,.8,.8])
    fig.add_axes(ax)
    for alpha_index in range(len(vf._alphaVectors)):

        l = Line2D([0,1],
                [vf._alphaVectors[alpha_index][state_indexes[0]],vf._alphaVectors[alpha_index][state_indexes[1]]])
        action_index = vf._alphaVectorActions[alpha_index]
        l.set_color(_Action_color[action_index])
        l.set_label("%d" %action_index)

        ax.add_line(l)

    alphaVectors_np = numpy.array(vf._alphaVectors)
    ax.set_ylim(ymin=alphaVectors_np.min(),ymax=alphaVectors_np.max())
    ax.set_xlim(xmin=0,xmax=1)
    ax.set_xlabel('Beliefspace' + str(state_indexes))
    ax.set_ylabel('Value')

    plt.legend()
    plt.show()


def plot_ValueFunction_3D(vf,state_indexes=[0,1,2]):
    """
    plot alpha vectors using mplot3d
    (http://matplotlib.sourceforge.net/mpl_toolkits/mplot3d/)
    arguments:
        value_function [pypomdp.ValueFunction]
        state_indexes - 3 state indexes (default: [0,1,2])
    """

    fig = plt.figure()
    ax = mpl_toolkits.mplot3d.Axes3D(fig)
    ax.auto_scale_xyz(X=True,Y=True,Z=True)
    ax.w_zaxis.set_scale("log")
    c = colors.ColorConverter()

    for alpha_index in range(len(vf._alphaVectors)):
        vtx = numpy.array([[0,1,vf._alphaVectors[alpha_index][state_indexes[0]]], \
            [0,0,vf._alphaVectors[alpha_index][state_indexes[1]]], \
            [1,0,vf._alphaVectors[alpha_index][state_indexes[2]]]])

        tri = mpl_toolkits.mplot3d.art3d.Poly3DCollection([vtx])

        action_index = vf._alphaVectorActions[alpha_index]
        tri.set_color(_Action_color[action_index])
        tri.set_label("%d" %action_index)

        ax.add_collection3d(tri)

    alphaVectors_np = numpy.array(vf._alphaVectors)
    ax.set_zlim3d(0,alphaVectors_np.max())
    ax.set_xlim3d(0,1)
    ax.set_ylim3d(0,1)
    ax.set_xlabel("Beliefspace [%d : %d]" % (state_indexes[0],state_indexes[1]))
    ax.set_ylabel("Beliefspace [%d : %d]" % (state_indexes[1],state_indexes[2]))
    ax.set_zlabel("Value")

    plt.legend()
    plt.show()
