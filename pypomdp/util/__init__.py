# -*- coding: utf-8 -*-
"""
    pypomdp.util
    ~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import collections

def initialize(obj,**kwargs):
    """
    Initializes an object with the keyword arguments provided in the call.
    """
    for k,v in kwargs.items():
        obj.__dict__[k] = v

def num_in_iter(iter_,obj):
    """
    Returns the number of times an object is in an iterator object by comparing
    the items' IDs with the object's ID.

    :param iter_: iterator object.
    :param obj: object
    :rtype: int
    """
    fpred = lambda x: id(x) == id(obj)
    return len(filter(fpred,iter_))

def in_iter(iter_,obj):
    """
    Checks whether or not is returned by an iterator by comparing the IDs of
    the returned objects with the ID of the object.

    :param iter_: iterator object.
    :param obj: object
    :rtype: bool
    """
    return not (num_in_iter(iter_,obj) == 0)

def call_count(method):
    """
    Instance method decorator that counts the number of times a function is
    called. It stores the counter for every registered method in a dictionary
    in the object's dict at 'funccall_count'. The call count only occurs if the
    instance or it's class has an attribute 'DEBUG' set to True.

    :param method: instance method.
    :rtype: decorated instance method.
    """
    def inner(obj,*args,**kwargs):
        key = str(method)
        try:
            debug = getattr(obj,"DEBUG")
        except AttributeError:
            debug = False
        if debug:
            try:
                obj.__dict__['_CC_'][key] += 1
            except KeyError:
                try:
                    obj.__dict__['_CC_'][key] = 1
                except KeyError:
                    obj.__dict__['_CC_'] = {}
                    obj.__dict__['_CC_'][key] = 1
        res = method(obj,*args,**kwargs)
        return res
    return inner

def to_list(data):
    """
    Function that converts a list, set, map or tuple to a new list, a single
    value to a singleton list and anything else to an empty list.

    :param data: data
    :rtype: list
    """
    if type(data) in (list,set,map,tuple):
        return list(data)
    if data:
        return [data]
    return []

def map_(funcs,arg):
    """
    Maps different functions over the same argument.

    :param funcs: functions to map over the argument.
    :param arg: argument to call functions with.
    """
    return [func(arg) for func in funcs]

def convert_to_default_dict(dict_,def_=lambda: None):
    d = collections.defaultdict(def_)
    for k,v in dict_.items():
        d[k] = v
    return d

def add_none_elems(dict_,attrs):
    missing = [k for k in attrs if k not in dict_.keys()]
    for m in missing:
        dict_[m] = None
    return dict_

def create_null_array(null,*args):
    if not all(map(lambda x: type(x) == int,args)):
        raise ValueError("Dimensions must be integers!")
    if filter(lambda x: x < 1,args):
        raise ValueError("Dimensions must be greater or equal to 1!")
    l = []
    if len(args) == 1:
        l = [null]*int(args[0])
    else:
        for n in range(args[0]): l.append(create_null_array(null,*args[1:]))
    return l

def check_matrix_dimensions(mx,*dimensions):
    """
    Checks whether a multi-dimensional matrix has the correct dimensions. If 
    the dimensions are not correct a ValueError is raised.

    :param mx: matrix
    :type mx: N-dimensions list
    :param dimensions: dimensions
    :type dimensions: int

    :return: None

    """
    curr,rest = dimensions[0],dimensions[1:]

    if len(mx) != curr:
        raise ValueError('incorrect matrix dimension %u (should be %u)' % 
                (len(mx),curr))
    elif len(rest) > 0:
        for row in mx:
            check_matrix_dimensions(row,*rest)
    return

def add_row_to_matrix(val,matrix):
    row_length = len(matrix[0])
    new_row = create_null_array(val,row_length)
    matrix.append(new_row)
    return matrix

def add_col_to_matrix(val,matrix):
    for row in matrix:
        row.append(val)
    return matrix

def not_func(func):
    """
    Return the inverse of a boolean function (useful as a decorator).

    :param func: function returning a boolean
    :type func: callable

    :return: function that will return the inverse boolean of the original
             function
    :rtype: callable
    """
    def inner(*args,**kwargs):
        return not func(*args,**kwargs)
    return inner

def arg_max(iter_):
    """
    Return the index of the maximum value in a list.
    :param iter_: iterable

    :return: index 
    :rtype: index
    """
    return arg_max_(iter_)[0]

def arg_max_(iter_):
    """
    Return the index of the maximum value in a list and the maximum value.
    :param iter_: iterable

    :return: index and maximum value
    :rtype: tuple(index,value)
    """
    n = -1
    v = float('-inf')
    for n_,v_ in enumerate(iter_):
        if v_ > v:
            n = n_
            v = v_

    return n,v

def arg_max_func(func,args):
    """
    Return the argument that produces the greatest result when applied to the
    given function.
    
    :param func: function to apply the arguments to
    :type func: callable
    :param args: list of argument tuples to be applied (not if the function
                 takes a single argument, the args may simply be a list
                 instead of a list of tuples containing a single element)
    :type args: tuple()

    :return: the arg max argument 
    """
    return arg_max_func_(func,args)[0]

def arg_max_func_(func,args):
    """
    Return the argument that produce the greatest result when applied to the
    given function and the value.
    
    :param func: function to apply the arguments to
    :type func: callable
    :param args: list of argument tuples to be applied (not if the function
                 takes a single argument, the args may simply be a list
                 instead of a list of tuples containing a single element)
    :type args: tuple()

    :return: the arg max argument and value
    :rtype: tuple(argument,value)
    """
    if type(args[0]) is not tuple:
        args = map(lambda a: (a,),args)
    results = map(lambda as_: func(*as_),args)
    arg,max_ = arg_max_(results)
    arg = args[arg]
    return arg,max_


def normalize(lst,val=None):
    """
    Normalize a list of values.

    :param lst: list of values
    :type  lst: numbers that are castable to doubles

    :param val: normalization value (i.e. required sum after normalization)
    :type  val: number that is castable to double
    
    :return: normalized list
    :rtype: list of doubles
    """

    val = float(val) or 1.0

    lst = map(float,lst)
    sum_ = sum(lst)

    assert sum_ > 0., "invalid belief state %s" % str(lst)

    lst = [v*val/sum_ for v in lst]
    return lst


def alist_to_dict(alist):
    dict_ = {}
    for k,v in alist:
        try:
            dict_[k].append(v)
        except KeyError:
            dict_[k] = [v]

    return dict_

