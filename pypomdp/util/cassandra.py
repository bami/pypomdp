# -*- coding: utf-8 -*-
"""
    pypomdp.util.cassandra
    ~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: cassandra utilities
    (http://www.pomdp.org/pomdp/code/pomdp-file-spec.shtml)
"""

import re
import sys
import numpy

def read_cassandra_alpha_vectors_from_file(filename):
    """
    Read cassandra policy (alpha vectors) from file

    :param filename: path to file 
    :type  filename: string 

    :return: dict mapping action index to list of alpha vectors
    :type: dict(int,[[double]])
    """
    with open(filename) as file_:
        policy_string = file_.read()
        return read_cassandra_alpha_vectors(policy_string)

def read_cassandra_alpha_policy(lines):
    """
    read cassandra policy file (alpha vectors) and return list of
    alpha vectors and corresponding action ids

    arguments:
        policy_string - name of the cassandra fle
    """
    alpha_vectors, alpha_vector_actions = [], []
    n_mdp_states = None

    for lineno,line in enumerate(lines):

        # ignore empty line
        if re.match("^\s*$", line):
            continue

        # ignore "# ..."
        elif re.match("^\s*#", line):
            continue

        # match a single number as action id
        elif re.match("^\s*\d+(\.\d+)?\s*$", line):
            mdp_values = re.split("\s*", line.rstrip('\n'))
            mdp_values = map(int,mdp_values)
            alpha_vector_actions.append(mdp_values[0])

        # match vector lines as MDP values
        elif re.match("^\s*(\-?\d+(\.\-?\d+)?\s*)+$", line):
            l = line.rstrip('\n')
            # split by space and remove Nones
            mdp_values = filter(None, re.split("\s+", l))
            mdp_values = map(float,mdp_values)

            # sanity check
            if not n_mdp_states:
                n_mdp_states = len(mdp_values)
            else:
                assert len(mdp_values) == n_mdp_states, \
                "invalid alpha vector length %i" % len(mdp_values)

            alpha_vectors.append(mdp_values)

        else:
            formatted_line = line.rstrip('\n')
            raise Exception("Error parsing line %u: `%s'" % (lineno,formatted_line))

    return alpha_vector_actions, alpha_vectors

def read_cassandra_alpha_vectors(policy_string):
    """
    Read cassandra policy string (alpha vectors)

    :param policy_string: string containing alpha vectors 
    :type  policy_string: string 

    :return: dict mapping action index to list of alpha vectors
    :type: dict(int,[[double]])
    """
    actions,alphas = read_cassandra_alpha_policy_(policy_string)
    alpha_vectors = {}

    for action, alpha in zip(actions,alphas):
        try:
            alpha_vectors[action].append(alpha)
        except KeyError:
            alpha_vectors[action] = [alpha]

    return alpha_vectors


def read_cassandra_policy_graph(policy_string):
    """
    read Cassandra policy graph (pg) and return transition and action list, as
    defined in pypomdp.PolicyGraph
    """

    lines = []
    for line in policy_string:
        # ignore empty line
        if re.match("^\s*$", line):
            continue
        # ignore "# ..."
        elif re.match("^\s*#", line):
            continue
        lines.append(line)

    line = re.sub(r"\s*\n",'', lines[0])
    line = re.sub(r"\s+",' ', line)
    observation_count = len(line.split(' ')) - 2
    action_count =  len(lines)
    transitions = [[None] * observation_count ] * action_count
    actions = [None] * action_count

    for line in lines:
        line = re.sub(r"\s*\n",'', line)
        values = line.split(' ')
        state_index = int(values[0])
        action_index = int(values[1])
        transition = [int(x) for x in values[3:]]
        actions[state_index] = action_index

        for observation_index in range(len(transition)):
            transitions[state_index][observation_index] = transition[observation_index]

    return transitions, actions


def write_cassandra(model,file_handle=None):
    """
    Write a POMDP in Cassandra format to file (or rather a file handle).

    :param model: POMDP
    :type model: :py:class:`pypomdp.core.POMDP`
    :param fileHandle: file to write to
    :type fileHandle: file-like object
    """

    fileHandle = file_handle or sys.stdout

    tpGen = ((a,s,s_)   for a  in xrange(model.num_actions)
                        for s  in xrange(model.num_states)
                        for s_ in xrange(model.num_states))

    obGen = ((a,s,o)    for a  in xrange(model.num_actions)
                        for s  in xrange(model.num_states)
                        for o  in xrange(model.num_observations))

    rwGen = ((a,o,s,s_) for a  in xrange(model.num_actions)
                        for o  in xrange(model.num_observations)
                        for s  in xrange(model.num_states)
                        for s_ in xrange(model.num_states))

    tpStr = "T:{action:d}:{source:d}:{sink:d} {prob:f}".format
    obStr = "O:{action:d}:{sink:d}:{observation:d} {prob:f}".format
    rwStr = "R:{action:d}:{source:d}:{sink:d}:{observation:d} {reward:f}".format

    
    writeline = lambda x: fileHandle.write(x+'\n')

    try:
        # write discount factor
        writeline("discount: %f" % model.discount)
    # If discount not defined
    except AttributeError:
        pass

    try:
        # write value type
        writeline("values: %s" % model.values)
    # If values not defined
    except AttributeError:
        pass

    # write state names
    writeline("states: %s" % ' '.join(model.state_names)) 
        
    # write action names
    writeline("actions: %s" % ' '.join(model.action_names))

    # write observation names
    writeline("observations: %s" % ' '.join(model.observation_names))
        
    # write transition probabilities
    writeline("# transition probabilities")
    writeline("# %s" % tpStr.__self__)
    for action,source,sink in tpGen:
        prob = model.tps[action][source][sink]
        writeline(tpStr(action=action,source=source,sink=sink,prob=prob))

    # write observation probabilities
    writeline("# observation probabilities")
    writeline("# %s" % obStr.__self__)
    for action,sink,observation in obGen:
        
        prob = model.ops[action][sink][observation]
        writeline(obStr(action=action,sink=sink,
            observation=observation,prob=prob))

    # write rewards
    writeline("# rewards")
    writeline("# %s" % rwStr.__self__)
    for action,observation,source,sink in rwGen:
        reward = model.rws[action][observation][source][sink]
        writeline(rwStr(action=action,source=source,sink=sink,
            observation=observation,reward=reward))


