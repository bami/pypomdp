# -*- coding: utf-8 -*-
"""
    pypomdp.parser
    ~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    :description: POMDP file-parser.

    This module implements a Cassandra file-format and a JSON parser
    to load POMDP models from files and returns an instance of
    pypomdp.model.POMDP.
"""
import re
import sys
import string
import collections

import copy


import pypomdp.util as util

_DEBUG = False
__MODULE__ = sys.modules[__name__]

class ParserError(BaseException):
    pass


# Regexes
# -----------------------------------------------------------------------------
tp_rx_str = r"(?P<type>T)( )*:( )*(?P<action>(\d+|\*))( )*:( )*(?P<source>(\d+|\*))( )*:( )*(?P<sink>(\d+|\*))( )+(?P<value>(\d+(\.\d+)?))( )*$"
tp_rx = re.compile(tp_rx_str)

ob_rx_str = r"(?P<type>O)( )*:( )*(?P<action>(\d+|\*))( )*:( )*(?P<sink>(\d+|\*))( )*:( )*(?P<observation>(\d+|\*))( )+(?P<value>(\d+(\.\d+)?))( )*$"
ob_rx = re.compile(ob_rx_str)

rw_rx_str = r"(?P<type>R)( )*:( )*(?P<action>(\d+|\*))( )*:( )*(?P<source>(\d+|\*))( )*:( )*(?P<sink>(\d+|\*))( )*:( )*(?P<observation>(\d+|\*))( )+(?P<value>((\+|\-)?\d+(\.\d+)?))( )*$"
rw_rx = re.compile(rw_rx_str)

states_rx_str = r"(?P<type>states): (?P<value>((\d+)|(\w+( )+)*(\w+)))( )*$"
states_rx = re.compile(states_rx_str)
states_sps = {"value": lambda x: filter(lambda y: y not in string.whitespace,x.split(' ')) 
        if ' ' in x else x}

actions_rx_str = r"(?P<type>actions): (?P<value>((\d+)|(\w+( )+)*(\w+)))( )*$"
actions_rx = re.compile(actions_rx_str)
actions_sps = {"value": lambda x: filter(lambda y: y not in string.whitespace,x.split(' ')) 
        if ' ' in x else x}

observations_rx_str = r"(?P<type>observations): (?P<value>((\d+)|(\w+( )+)*(\w+)))( )*$"
observations_rx = re.compile(observations_rx_str)
observations_sps = {"value": lambda x: filter(lambda y: y not in string.whitespace,x.split(' '))
        if ' ' in x else x}

discount_rx_str = r"(?P<type>discount)( )*:( )*(?P<value>(\d+(.\d+)*))( )*$"
discount_rx = re.compile(discount_rx_str)

values_rx_str = r"(?P<type>values)( )*:( )*(?P<value>(reward|cost))( )*$"
values_rx = re.compile(values_rx_str)

# Parsing
# -----------------------------------------------------------------------------
def get_rxs_sps_in_here():
    rxs = {}
    sps = {}
    for k in __MODULE__.__dict__:
        if   k.endswith("_rx"):
            rxs[k[:-3]] = __MODULE__.__dict__[k]
        elif k.endswith("_sps"):
            sps[k[:-4]] = __MODULE__.__dict__[k]
    return (rxs,sps)

def match_lines(lines):
    rxs,sps = get_rxs_sps_in_here()
    results = []
    line_number = 0
    for line in lines:
        line_number += 1
        for regex_name,regex in rxs.iteritems():
            sub_parsers = sps.get(regex_name)
            try:
                results.append(match(line,regex,sub_parsers))
                break
            except ValueError:
                pass
        if len(results) != line_number:
            results.append(None)
    return results

def verbose_match_lines(lines):
    results = match_lines(lines)
    for n,result in enumerate(results):
        print "line %i: `%s'\n-> %s" % (n+1,lines[n],result)
    return results

def match(str,rx,sub_parsers=None,custom_type=None):
    match_obj = rx.match(str)
    if match_obj is None:
        raise ValueError("Unable to match string `%s' with pattern `%s'" % (str,rx.pattern))
    groups = match_obj.groupdict()
    if sub_parsers:
        for group_name,parser in sub_parsers.iteritems():
            try:
                groups[group_name] = parser(groups[group_name])
            except KeyError:
                pass
    if custom_type:
        groups["type"] = custom_type
    return groups

# Model Creation
# -----------------------------------------------------------------------------

def get_num_items(parsed_model,item_type):
    try:
        items_item = filter(lambda x: x["type"] == item_type,parsed_model)[0]
        if type(items_item["value"]) is list:
            return len(items_item["value"])
        else:
            return int(items_item["value"])
    except (KeyError,IndexError):
        raise ValueError("Unable to get number of %s!" % item_type)

def get_num_states(parsed_model):
    return get_num_items(parsed_model,"states")

def get_num_actions(parsed_model):
    return get_num_items(parsed_model,"actions")

def get_num_observations(parsed_model):
    return get_num_observations(parsed_model,"observations")

def create_empty_model(parsed_model):
    pass

# File Parsing
# -----------------------------------------------------------------------------
def is_whitespace(line):
    return all(map(lambda x: x in string.whitespace,line))

def is_comment(line):
    return line.strip().startswith("#")



class Model(object):
    def __init__(self,states,actions,observations,discount,values):

        if type(states) is int:
            self.state_names = ["state"+str(i) for i in xrange(states)]
        else:
            self.state_names = states
        self.num_states = len(self.state_names)

        if type(actions) is int:
            self.action_names = ["action"+str(i) for i in xrange(actions)]
        else:
            self.action_names = actions
        self.num_actions = len(self.action_names)

        if type(observations) is int:
            self.observation_names = ["observation"+str(i) for i in xrange(observations)]
        else:
            self.observation_names = observations
        self.num_observations = len(self.observation_names)

        self.discount = discount
        self.values = values

        # Transition probabilities:
        # [action][source][sink]
        self.transition_probabilities = \
                util.create_null_array(0.0,self.num_actions,self.num_states,self.num_states)
        # Observation probabilities:
        # [action][sink][observation]
        self.observation_probabilities = \
                util.create_null_array(0.0,self.num_actions,self.num_states,self.num_observations)
        # Rewards:
        # [action][source][sink][observation]
        self.rewards = \
                util.create_null_array(0.0,self.num_actions,self.num_states,self.num_states
                        ,self.num_observations)

    def set_tp(self,action,source,sink,value):
        action,source,sink = map(int,(action,source,sink))
        value = float(value)
        if _DEBUG: print("TP(%i,%i,%i)\t<- %f" % (action,source,sink,value))
        self.transition_probabilities[action][source][sink] = value

    def set_ob(self,action,sink,observation,value):
        action,sink,observation = map(int,(action,sink,observation))
        value = float(value)
        if _DEBUG: print("OB(%i,%i,%i)\t<- %f" % (action,sink,observation,value))
        self.observation_probabilities[action][sink][observation] = value

    def set_rw(self,action,source,sink,observation,value):
        action,observation,source,sink = map(int,(action,observation,source,sink))
        value = float(value)
        if _DEBUG: print("RW(%i,%i,%i,%i)\t<- %f" % (action,source,sink,observation,value))
        self.rewards[action][source][sink][observation] = value


def get_line_by_type(parsed_lines,type_):
    try:
        return filter(lambda x: x.get("type") == type_,parsed_lines)[0]
    except IndexError:
        raise ValueError("Unable to find line with type `%s'!" % type_)

def create_empty_model(parsed_lines):
    states = get_line_by_type(parsed_lines,"states")["value"]
    if type(states) is not list:
        states = int(states)

    actions = get_line_by_type(parsed_lines,"actions")["value"]
    if type(actions) is not list:
        actions = int(actions)

    observations = get_line_by_type(parsed_lines,"observations")["value"]
    if type(observations) is not list:
        observations = int(observations)

    discount = get_line_by_type(parsed_lines,"discount")["value"]
    discount = float(discount)

    values = get_line_by_type(parsed_lines,"values")["value"]

    return Model(states,actions,observations,discount,values)

def fill_model(empty_model,parsed_lines):
    tps = filter(lambda x: x.get("type") == "T",parsed_lines)
    obs = filter(lambda x: x.get("type") == "O",parsed_lines)
    rws = filter(lambda x: x.get("type") == "R",parsed_lines)

    def set_tp(tp):
        if type(tp) is dict: vals = map(lambda k: tp[k], ["action","source","sink","value"])
        else: vals = tp
        idx_getter = ["num_actions","num_states","num_states"]
        if "*" not in vals: empty_model.set_tp(*vals)
        else:
            pos = vals.index("*")
            orig_vals = copy.copy(vals)
            for i in xrange(getattr(empty_model,idx_getter[pos])):
                vals = orig_vals
                vals[pos] = i
                set_tp(vals)
    for tp in tps:
        set_tp(tp)

    def set_ob(ob):
        if type(ob) is dict: vals = map(lambda k: ob[k], ["action","sink","observation","value"])
        else: vals = ob
        idx_getter = ["num_actions","num_states","num_observations"]
        if "*" not in vals: empty_model.set_ob(*vals)
        else:
            pos = vals.index("*")
            orig_vals = copy.copy(vals)
            for i in xrange(getattr(empty_model,idx_getter[pos])):
                vals = orig_vals
                vals[pos] = i
                set_ob(vals)

    for ob in obs:
        set_ob(ob)

    def set_rw(rw):
        if type(rw) is dict: vals = map(lambda k: rw[k], ["action","source","sink","observation","value"])
        else: vals = rw
        idx_getter = ["num_actions","num_observations","num_states","num_states"]
        if "*" not in vals: empty_model.set_rw(*vals)
        else:
            pos = vals.index("*")
            orig_vals = copy.copy(vals)
            for i in xrange(getattr(empty_model,idx_getter[pos])):
                vals = orig_vals
                vals[pos] = i
                set_rw(vals)

    for rw in rws:
        set_rw(rw)

    return empty_model

def model_from_string(str):
    lines = str.splitlines()
    parsed_lines = parse_lines(lines)
    model = create_empty_model(parsed_lines)
    model = fill_model(model,parsed_lines)
    return model

def model_from_file(fname):
    with open(fname,'r') as f:
        lines = f.readlines()
    parsed_lines = parse_lines(lines)
    model = create_empty_model(parsed_lines)
    model = fill_model(model,parsed_lines)
    return model

def parse_lines(lines):

    # strip leading/trailing whitespace
    lines = map(string.strip,lines)

    # remove blank lines
    lines = filter(util.not_func(is_whitespace),lines)
    
    # remove comments
    lines = filter(util.not_func(is_comment),lines)

    if _DEBUG:
        parsed_lines = verbose_match_lines(lines)
    else:
        parsed_lines = match_lines(lines)

    if not all(parsed_lines):
        error_lines = []
        for i,l in enumerate(parsed_lines):
            if l is None:
                error_lines.append(i+1)
        error_lines = map(str,error_lines)
        raise ParserError("Error parsing lines: %s!" % ','.join(error_lines))
    else:
        if _DEBUG:
            print("All lines parsed successfully!")

    return parsed_lines

if __name__ == "__main__":
    import sys
    fname = sys.argv[1]
    model_from_file(fname)
