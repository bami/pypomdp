.. pyPOMDP documentation master file, created by
   sphinx-quickstart on Fri Mar 22 14:47:35 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyPOMDP's documentation!
===================================

pyPOMDP is a POMDP implementation for Python 2.x.

The following example show how the contoller and the simulator can be used. 

First, the POMDP **model** is read from file (either Cassandra's file format or a JSON file).

The **controller** returns a control action on sending an observation (senseact()). 
The control action is selected according to it's current belief state and **policy**. 
The policy is also read from a file (so far no online planning).

The **simulator** samples the current system state internally from the process model with respect
to the applied action. Based on that, it records the reward and emmits a sampled observation
to the controller.

See the following examples for implementation details:

.. literalinclude:: ../examples/shuttle_demo.py

Modules and Classes
===================


.. toctree::
   :maxdepth: 2

Model
*****
.. autoclass:: pypomdp.model.POMDP
    :members:

Control Policy
**************
.. autoclass:: pypomdp.policy.Policy
    :members:

.. autoclass:: pypomdp.policy.Policy
    :members:

Controller
**********

.. autoclass:: pypomdp.control.Controller
    :members:
.. autoclass:: pypomdp.control.ProdController
    :members:

Simulator
*********
.. autoclass:: pypomdp.simulator.Simulator
    :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

