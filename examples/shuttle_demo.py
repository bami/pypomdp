# -*- coding: utf-8 -*-
"""
    shuttle.95.demo
    ~~~~~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
    :description: this small demo shows how a pomdp controller
    runs a pomdp simulation. 
   
"""


import time
import pypomdp

def main():
    # model
    model = None

    # read from JSON
    model_filename = './examples/models/shuttle.95.JSON'
    with open(model_filename, 'r') as fh:
        model = pypomdp.model.POMDP.read_json(fh)

    # read from Cassandra
    #model = pypomdp.model.POMDP.read_cassandra('./examples/models/shuttle.95.explicit.POMDP')

    # simulator
    simulator = pypomdp.simulator.Simulator(pomdp=model)

    # controller
    policy_filename = './examples/models/shuttle.95.alpha'
    policy = pypomdp.policy.AlphaVectorValueFunction.\
            initFromCassandraFile(policy_filename, model)
    controller = pypomdp.control.ProdController(model=model, policy=policy)

    # run system
    action = controller.last_action
    while(True):
        time.sleep(1)
        reward, observ = simulator.simulate(action)
        action = controller.senseact(observ)

if __name__ == "__main__":
    main()
